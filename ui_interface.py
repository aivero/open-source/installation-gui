# -*- coding: utf-8 -*-

################################################################################
## Form generated from reading UI file 'interfaceaMnIVa.ui'
##
## Created by: Qt User Interface Compiler version 5.15.3
##
## WARNING! All changes made in this file will be lost when recompiling UI file!
################################################################################

from PySide2.QtCore import *
from PySide2.QtGui import *
from PySide2.QtWidgets import *

from Custom_Widgets.Widgets import QCustomStackedWidget
from Custom_Widgets.Widgets import QCustomSlideMenu
from Custom_Widgets.Widgets import FormProgressIndicator

import resources_rc

class Ui_MainWindow(object):
    def setupUi(self, MainWindow):
        if not MainWindow.objectName():
            MainWindow.setObjectName(u"MainWindow")
        MainWindow.resize(671, 407)
        MainWindow.setMaximumSize(QSize(16777215, 2035))
        palette = QPalette()
        brush = QBrush(QColor(255, 255, 255, 255))
        brush.setStyle(Qt.SolidPattern)
        palette.setBrush(QPalette.Active, QPalette.WindowText, brush)
        brush1 = QBrush(QColor(19, 19, 20, 255))
        brush1.setStyle(Qt.SolidPattern)
        palette.setBrush(QPalette.Active, QPalette.Button, brush1)
        palette.setBrush(QPalette.Active, QPalette.Light, brush)
        palette.setBrush(QPalette.Active, QPalette.Text, brush)
        palette.setBrush(QPalette.Active, QPalette.ButtonText, brush)
        palette.setBrush(QPalette.Active, QPalette.Base, brush1)
        palette.setBrush(QPalette.Active, QPalette.Window, brush1)
        brush2 = QBrush(QColor(201, 51, 101, 255))
        brush2.setStyle(Qt.SolidPattern)
        palette.setBrush(QPalette.Active, QPalette.Highlight, brush2)
        brush3 = QBrush(QColor(255, 255, 255, 128))
        brush3.setStyle(Qt.SolidPattern)
#if QT_VERSION >= QT_VERSION_CHECK(5, 12, 0)
        palette.setBrush(QPalette.Active, QPalette.PlaceholderText, brush3)
#endif
        palette.setBrush(QPalette.Inactive, QPalette.WindowText, brush)
        palette.setBrush(QPalette.Inactive, QPalette.Button, brush1)
        palette.setBrush(QPalette.Inactive, QPalette.Light, brush)
        palette.setBrush(QPalette.Inactive, QPalette.Text, brush)
        palette.setBrush(QPalette.Inactive, QPalette.ButtonText, brush)
        palette.setBrush(QPalette.Inactive, QPalette.Base, brush1)
        palette.setBrush(QPalette.Inactive, QPalette.Window, brush1)
        palette.setBrush(QPalette.Inactive, QPalette.Highlight, brush2)
#if QT_VERSION >= QT_VERSION_CHECK(5, 12, 0)
        palette.setBrush(QPalette.Inactive, QPalette.PlaceholderText, brush3)
#endif
        brush4 = QBrush(QColor(190, 190, 190, 255))
        brush4.setStyle(Qt.SolidPattern)
        palette.setBrush(QPalette.Disabled, QPalette.WindowText, brush4)
        palette.setBrush(QPalette.Disabled, QPalette.Button, brush1)
        palette.setBrush(QPalette.Disabled, QPalette.Light, brush)
        palette.setBrush(QPalette.Disabled, QPalette.Text, brush4)
        palette.setBrush(QPalette.Disabled, QPalette.ButtonText, brush4)
        palette.setBrush(QPalette.Disabled, QPalette.Base, brush1)
        palette.setBrush(QPalette.Disabled, QPalette.Window, brush1)
        brush5 = QBrush(QColor(145, 145, 145, 255))
        brush5.setStyle(Qt.SolidPattern)
        palette.setBrush(QPalette.Disabled, QPalette.Highlight, brush5)
        brush6 = QBrush(QColor(0, 0, 0, 128))
        brush6.setStyle(Qt.SolidPattern)
#if QT_VERSION >= QT_VERSION_CHECK(5, 12, 0)
        palette.setBrush(QPalette.Disabled, QPalette.PlaceholderText, brush6)
#endif
        MainWindow.setPalette(palette)
        MainWindow.setStyleSheet(u"")
        self.centralwidget = QWidget(MainWindow)
        self.centralwidget.setObjectName(u"centralwidget")
        palette1 = QPalette()
        self.centralwidget.setPalette(palette1)
        self.verticalLayout = QVBoxLayout(self.centralwidget)
        self.verticalLayout.setSpacing(0)
        self.verticalLayout.setObjectName(u"verticalLayout")
        self.verticalLayout.setContentsMargins(0, 0, 0, 0)
        self.header_frame = QFrame(self.centralwidget)
        self.header_frame.setObjectName(u"header_frame")
        palette2 = QPalette()
        self.header_frame.setPalette(palette2)
        self.header_frame.setFrameShape(QFrame.StyledPanel)
        self.header_frame.setFrameShadow(QFrame.Raised)
        self.horizontalLayout = QHBoxLayout(self.header_frame)
        self.horizontalLayout.setSpacing(0)
        self.horizontalLayout.setObjectName(u"horizontalLayout")
        self.horizontalLayout.setContentsMargins(0, 0, 0, 0)
        self.license_frame = QFrame(self.header_frame)
        self.license_frame.setObjectName(u"license_frame")
        self.license_frame.setFrameShape(QFrame.StyledPanel)
        self.license_frame.setFrameShadow(QFrame.Raised)
        self.license_button = QPushButton(self.license_frame)
        self.license_button.setObjectName(u"license_button")
        self.license_button.setEnabled(True)
        self.license_button.setGeometry(QRect(10, 10, 37, 31))
        palette3 = QPalette()
        self.license_button.setPalette(palette3)
        icon = QIcon()
        icon.addFile(u"icons/info.png", QSize(), QIcon.Normal, QIcon.Off)
        self.license_button.setIcon(icon)
        self.license_button.setIconSize(QSize(25, 25))

        self.horizontalLayout.addWidget(self.license_frame)

        self.window_title = QLabel(self.header_frame)
        self.window_title.setObjectName(u"window_title")
        palette4 = QPalette()
        self.window_title.setPalette(palette4)
        font = QFont()
        font.setPointSize(14)
        font.setBold(True)
        font.setWeight(75)
        self.window_title.setFont(font)
        self.window_title.setAlignment(Qt.AlignCenter)
        self.window_title.setWordWrap(False)

        self.horizontalLayout.addWidget(self.window_title)

        self.window_closing_frame = QFrame(self.header_frame)
        self.window_closing_frame.setObjectName(u"window_closing_frame")
        palette5 = QPalette()
        self.window_closing_frame.setPalette(palette5)
        self.window_closing_frame.setFrameShape(QFrame.StyledPanel)
        self.window_closing_frame.setFrameShadow(QFrame.Raised)
        self.horizontalLayout_4 = QHBoxLayout(self.window_closing_frame)
        self.horizontalLayout_4.setObjectName(u"horizontalLayout_4")
        self.minimize_window_button = QPushButton(self.window_closing_frame)
        self.minimize_window_button.setObjectName(u"minimize_window_button")
        palette6 = QPalette()
        self.minimize_window_button.setPalette(palette6)
        icon1 = QIcon()
        icon1.addFile(u":/icons/icons/minus-circle.png", QSize(), QIcon.Normal, QIcon.Off)
        self.minimize_window_button.setIcon(icon1)
        self.minimize_window_button.setIconSize(QSize(22, 22))

        self.horizontalLayout_4.addWidget(self.minimize_window_button)

        self.exit_button = QPushButton(self.window_closing_frame)
        self.exit_button.setObjectName(u"exit_button")
        self.exit_button.setEnabled(True)
        palette7 = QPalette()
        self.exit_button.setPalette(palette7)
        icon2 = QIcon()
        icon2.addFile(u":/icons/icons/x-circle.png", QSize(), QIcon.Normal, QIcon.Off)
        self.exit_button.setIcon(icon2)
        self.exit_button.setIconSize(QSize(25, 25))

        self.horizontalLayout_4.addWidget(self.exit_button)


        self.horizontalLayout.addWidget(self.window_closing_frame, 0, Qt.AlignRight)

        self.window_title.raise_()
        self.window_closing_frame.raise_()
        self.license_frame.raise_()

        self.verticalLayout.addWidget(self.header_frame, 0, Qt.AlignTop)

        self.main_body = QFrame(self.centralwidget)
        self.main_body.setObjectName(u"main_body")
        sizePolicy = QSizePolicy(QSizePolicy.Preferred, QSizePolicy.Preferred)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.main_body.sizePolicy().hasHeightForWidth())
        self.main_body.setSizePolicy(sizePolicy)
        self.main_body.setMinimumSize(QSize(671, 354))
        self.main_body.setMaximumSize(QSize(671, 354))
        palette8 = QPalette()
        self.main_body.setPalette(palette8)
        self.main_body.setFrameShape(QFrame.StyledPanel)
        self.main_body.setFrameShadow(QFrame.Raised)
        self.verticalLayout_2 = QVBoxLayout(self.main_body)
        self.verticalLayout_2.setSpacing(0)
        self.verticalLayout_2.setObjectName(u"verticalLayout_2")
        self.verticalLayout_2.setContentsMargins(0, 0, 0, 0)
        self.exit_container = QCustomSlideMenu(self.main_body)
        self.exit_container.setObjectName(u"exit_container")
        self.exit_container.setMaximumSize(QSize(16777215, 0))
        palette9 = QPalette()
        self.exit_container.setPalette(palette9)
        self.verticalLayout_3 = QVBoxLayout(self.exit_container)
        self.verticalLayout_3.setSpacing(0)
        self.verticalLayout_3.setObjectName(u"verticalLayout_3")
        self.verticalLayout_3.setContentsMargins(0, 0, 0, 0)
        self.main_frame = QFrame(self.exit_container)
        self.main_frame.setObjectName(u"main_frame")
        self.main_frame.setMinimumSize(QSize(100, 200))
        palette10 = QPalette()
        self.main_frame.setPalette(palette10)
        self.main_frame.setFrameShape(QFrame.StyledPanel)
        self.main_frame.setFrameShadow(QFrame.Raised)
        self.verticalLayout_4 = QVBoxLayout(self.main_frame)
        self.verticalLayout_4.setObjectName(u"verticalLayout_4")
        self.welcome_message = QLabel(self.main_frame)
        self.welcome_message.setObjectName(u"welcome_message")
        palette11 = QPalette()
        self.welcome_message.setPalette(palette11)
        self.welcome_message.setAlignment(Qt.AlignCenter)

        self.verticalLayout_4.addWidget(self.welcome_message)

        self.welcome_info = QFrame(self.main_frame)
        self.welcome_info.setObjectName(u"welcome_info")
        palette12 = QPalette()
        self.welcome_info.setPalette(palette12)
        self.welcome_info.setFrameShape(QFrame.StyledPanel)
        self.welcome_info.setFrameShadow(QFrame.Raised)
        self.horizontalLayout_5 = QHBoxLayout(self.welcome_info)
        self.horizontalLayout_5.setSpacing(0)
        self.horizontalLayout_5.setObjectName(u"horizontalLayout_5")
        self.horizontalLayout_5.setContentsMargins(0, 0, 0, 0)
        self.close_exit_wizard = QPushButton(self.welcome_info)
        self.close_exit_wizard.setObjectName(u"close_exit_wizard")
        palette13 = QPalette()
        self.close_exit_wizard.setPalette(palette13)
        self.close_exit_wizard.setIcon(icon2)
        self.close_exit_wizard.setIconSize(QSize(50, 50))

        self.horizontalLayout_5.addWidget(self.close_exit_wizard, 0, Qt.AlignLeft)

        self.close_window_button = QPushButton(self.welcome_info)
        self.close_window_button.setObjectName(u"close_window_button")
        palette14 = QPalette()
        self.close_window_button.setPalette(palette14)
        icon3 = QIcon()
        icon3.addFile(u":/icons/icons/check-circle.png", QSize(), QIcon.Normal, QIcon.Off)
        self.close_window_button.setIcon(icon3)
        self.close_window_button.setIconSize(QSize(50, 50))

        self.horizontalLayout_5.addWidget(self.close_window_button, 0, Qt.AlignRight)


        self.verticalLayout_4.addWidget(self.welcome_info)


        self.verticalLayout_3.addWidget(self.main_frame, 0, Qt.AlignHCenter|Qt.AlignVCenter)


        self.verticalLayout_2.addWidget(self.exit_container)

        self.licensing_container = QCustomSlideMenu(self.main_body)
        self.licensing_container.setObjectName(u"licensing_container")
        self.licensing_container.setMaximumSize(QSize(16777215, 0))
        palette15 = QPalette()
        self.licensing_container.setPalette(palette15)
        self.verticalLayout_31 = QVBoxLayout(self.licensing_container)
        self.verticalLayout_31.setSpacing(0)
        self.verticalLayout_31.setObjectName(u"verticalLayout_31")
        self.verticalLayout_31.setContentsMargins(0, 0, 0, 0)
        self.main_frame1 = QFrame(self.licensing_container)
        self.main_frame1.setObjectName(u"main_frame1")
        self.main_frame1.setMinimumSize(QSize(100, 200))
        palette16 = QPalette()
        self.main_frame1.setPalette(palette16)
        self.main_frame1.setFrameShape(QFrame.StyledPanel)
        self.main_frame1.setFrameShadow(QFrame.Raised)
        self.verticalLayout_41 = QVBoxLayout(self.main_frame1)
        self.verticalLayout_41.setObjectName(u"verticalLayout_41")
        self.welcome_message1 = QLabel(self.main_frame1)
        self.welcome_message1.setObjectName(u"welcome_message1")
        palette17 = QPalette()
        self.welcome_message1.setPalette(palette17)
        self.welcome_message1.setAlignment(Qt.AlignCenter)
        self.welcome_message1.setWordWrap(True)
        self.welcome_message1.setTextInteractionFlags(Qt.TextBrowserInteraction)

        self.verticalLayout_41.addWidget(self.welcome_message1)

        self.licensing_frame = QFrame(self.main_frame1)
        self.licensing_frame.setObjectName(u"licensing_frame")
        palette18 = QPalette()
        self.licensing_frame.setPalette(palette18)
        self.licensing_frame.setFrameShape(QFrame.StyledPanel)
        self.licensing_frame.setFrameShadow(QFrame.Raised)
        self.horizontalLayout_51 = QHBoxLayout(self.licensing_frame)
        self.horizontalLayout_51.setSpacing(0)
        self.horizontalLayout_51.setObjectName(u"horizontalLayout_51")
        self.horizontalLayout_51.setContentsMargins(0, 0, 0, 0)

        self.verticalLayout_41.addWidget(self.licensing_frame)


        self.verticalLayout_31.addWidget(self.main_frame1, 0, Qt.AlignHCenter|Qt.AlignVCenter)


        self.verticalLayout_2.addWidget(self.licensing_container)

        self.main_contents = QCustomSlideMenu(self.main_body)
        self.main_contents.setObjectName(u"main_contents")
        sizePolicy.setHeightForWidth(self.main_contents.sizePolicy().hasHeightForWidth())
        self.main_contents.setSizePolicy(sizePolicy)
        self.main_contents.setMinimumSize(QSize(671, 354))
        self.main_contents.setMaximumSize(QSize(16777215, 16777215))
        palette19 = QPalette()
        self.main_contents.setPalette(palette19)
        self.verticalLayout_5 = QVBoxLayout(self.main_contents)
        self.verticalLayout_5.setSpacing(0)
        self.verticalLayout_5.setObjectName(u"verticalLayout_5")
        self.verticalLayout_5.setContentsMargins(0, 0, 0, 0)
        self.middle_widget = QFrame(self.main_contents)
        self.middle_widget.setObjectName(u"middle_widget")
        sizePolicy.setHeightForWidth(self.middle_widget.sizePolicy().hasHeightForWidth())
        self.middle_widget.setSizePolicy(sizePolicy)
        self.middle_widget.setMinimumSize(QSize(0, 0))
        palette20 = QPalette()
        self.middle_widget.setPalette(palette20)
        self.middle_widget.setFrameShape(QFrame.StyledPanel)
        self.middle_widget.setFrameShadow(QFrame.Sunken)
        self.horizontalLayout_7 = QHBoxLayout(self.middle_widget)
        self.horizontalLayout_7.setSpacing(0)
        self.horizontalLayout_7.setObjectName(u"horizontalLayout_7")
        self.horizontalLayout_7.setContentsMargins(0, 0, 0, -1)
        self.left_frame = QFrame(self.middle_widget)
        self.left_frame.setObjectName(u"left_frame")
        self.left_frame.setMinimumSize(QSize(200, 200))
        self.left_frame.setMaximumSize(QSize(200, 200))
        palette21 = QPalette()
        self.left_frame.setPalette(palette21)
        self.left_frame.setFrameShape(QFrame.NoFrame)
        self.left_frame.setFrameShadow(QFrame.Sunken)
        self.verticalLayout_8 = QVBoxLayout(self.left_frame)
        self.verticalLayout_8.setObjectName(u"verticalLayout_8")
        self.verticalLayout_8.setContentsMargins(0, 0, 0, 0)
        self.aivero_logo = QLabel(self.left_frame)
        self.aivero_logo.setObjectName(u"aivero_logo")
        sizePolicy1 = QSizePolicy(QSizePolicy.Fixed, QSizePolicy.Preferred)
        sizePolicy1.setHorizontalStretch(0)
        sizePolicy1.setVerticalStretch(0)
        sizePolicy1.setHeightForWidth(self.aivero_logo.sizePolicy().hasHeightForWidth())
        self.aivero_logo.setSizePolicy(sizePolicy1)
        self.aivero_logo.setMinimumSize(QSize(200, 200))
        palette22 = QPalette()
        self.aivero_logo.setPalette(palette22)
        self.aivero_logo.setFrameShadow(QFrame.Plain)
        self.aivero_logo.setPixmap(QPixmap(u":/icons/icons/Aivero_logo_facebook_170x170.svg"))
        self.aivero_logo.setScaledContents(True)

        self.verticalLayout_8.addWidget(self.aivero_logo)


        self.horizontalLayout_7.addWidget(self.left_frame)

        self.welcome = QCustomSlideMenu(self.middle_widget)
        self.welcome.setObjectName(u"welcome")
        sizePolicy.setHeightForWidth(self.welcome.sizePolicy().hasHeightForWidth())
        self.welcome.setSizePolicy(sizePolicy)
        self.welcome.setMaximumSize(QSize(469, 200))
        palette23 = QPalette()
        self.welcome.setPalette(palette23)
        self.verticalLayout_9 = QVBoxLayout(self.welcome)
        self.verticalLayout_9.setObjectName(u"verticalLayout_9")
        self.label_4 = QLabel(self.welcome)
        self.label_4.setObjectName(u"label_4")
        palette24 = QPalette()
        self.label_4.setPalette(palette24)
        font1 = QFont()
        font1.setPointSize(13)
        font1.setBold(True)
        font1.setWeight(75)
        self.label_4.setFont(font1)
        self.label_4.setAlignment(Qt.AlignCenter)

        self.verticalLayout_9.addWidget(self.label_4)

        self.label_5 = QLabel(self.welcome)
        self.label_5.setObjectName(u"label_5")
        palette25 = QPalette()
        self.label_5.setPalette(palette25)
        self.label_5.setAlignment(Qt.AlignCenter)

        self.verticalLayout_9.addWidget(self.label_5)

        self.start_wizard_btn = QPushButton(self.welcome)
        self.start_wizard_btn.setObjectName(u"start_wizard_btn")
        sizePolicy2 = QSizePolicy(QSizePolicy.Expanding, QSizePolicy.Fixed)
        sizePolicy2.setHorizontalStretch(0)
        sizePolicy2.setVerticalStretch(0)
        sizePolicy2.setHeightForWidth(self.start_wizard_btn.sizePolicy().hasHeightForWidth())
        self.start_wizard_btn.setSizePolicy(sizePolicy2)
        palette26 = QPalette()
        self.start_wizard_btn.setPalette(palette26)
        icon4 = QIcon()
        icon4.addFile(u":/icons/icons/arrow-right-circle.png", QSize(), QIcon.Normal, QIcon.Off)
        self.start_wizard_btn.setIcon(icon4)
        self.start_wizard_btn.setIconSize(QSize(44, 44))

        self.verticalLayout_9.addWidget(self.start_wizard_btn)


        self.horizontalLayout_7.addWidget(self.welcome)

        self.pages = QCustomSlideMenu(self.middle_widget)
        self.pages.setObjectName(u"pages")
        sizePolicy.setHeightForWidth(self.pages.sizePolicy().hasHeightForWidth())
        self.pages.setSizePolicy(sizePolicy)
        self.pages.setMinimumSize(QSize(0, 0))
        self.pages.setMaximumSize(QSize(16777215, 0))
        palette27 = QPalette()
        self.pages.setPalette(palette27)
        self.verticalLayout_10 = QVBoxLayout(self.pages)
        self.verticalLayout_10.setObjectName(u"verticalLayout_10")
        self.scrollArea = QScrollArea(self.pages)
        self.scrollArea.setObjectName(u"scrollArea")
        sizePolicy.setHeightForWidth(self.scrollArea.sizePolicy().hasHeightForWidth())
        self.scrollArea.setSizePolicy(sizePolicy)
        self.scrollArea.setMinimumSize(QSize(0, 0))
        palette28 = QPalette()
        self.scrollArea.setPalette(palette28)
        self.scrollArea.setWidgetResizable(False)
        self.scrollAreaWidgetContents = QWidget()
        self.scrollAreaWidgetContents.setObjectName(u"scrollAreaWidgetContents")
        self.scrollAreaWidgetContents.setGeometry(QRect(0, 0, 450, 250))
        self.scrollAreaWidgetContents.setMinimumSize(QSize(0, 0))
        palette29 = QPalette()
        self.scrollAreaWidgetContents.setPalette(palette29)
        self.verticalLayout_11 = QVBoxLayout(self.scrollAreaWidgetContents)
        self.verticalLayout_11.setSpacing(0)
        self.verticalLayout_11.setObjectName(u"verticalLayout_11")
        self.verticalLayout_11.setContentsMargins(0, 0, 0, 0)
        self.installation_pages = QCustomStackedWidget(self.scrollAreaWidgetContents)
        self.installation_pages.setObjectName(u"installation_pages")
        self.installation_pages.setMinimumSize(QSize(0, 0))
        palette30 = QPalette()
        self.installation_pages.setPalette(palette30)
        self.page_1 = QWidget()
        self.page_1.setObjectName(u"page_1")
        self.page_1.setMinimumSize(QSize(0, 0))
        self.verticalLayout_12 = QVBoxLayout(self.page_1)
        self.verticalLayout_12.setObjectName(u"verticalLayout_12")
        self.label_9 = QLabel(self.page_1)
        self.label_9.setObjectName(u"label_9")
        self.label_9.setMinimumSize(QSize(0, 0))
        palette31 = QPalette()
        self.label_9.setPalette(palette31)
        font2 = QFont()
        font2.setPointSize(18)
        font2.setBold(True)
        font2.setWeight(75)
        self.label_9.setFont(font2)
        self.label_9.setAlignment(Qt.AlignCenter)
        self.label_9.setWordWrap(True)

        self.verticalLayout_12.addWidget(self.label_9)

        self.label_16 = QLabel(self.page_1)
        self.label_16.setObjectName(u"label_16")
        sizePolicy.setHeightForWidth(self.label_16.sizePolicy().hasHeightForWidth())
        self.label_16.setSizePolicy(sizePolicy)
        self.label_16.setMinimumSize(QSize(0, 0))
        palette32 = QPalette()
        self.label_16.setPalette(palette32)
        font3 = QFont()
        font3.setPointSize(13)
        self.label_16.setFont(font3)
        self.label_16.setAlignment(Qt.AlignJustify|Qt.AlignVCenter)
        self.label_16.setWordWrap(True)

        self.verticalLayout_12.addWidget(self.label_16)

        self.go_to_page2 = QPushButton(self.page_1)
        self.go_to_page2.setObjectName(u"go_to_page2")
        sizePolicy2.setHeightForWidth(self.go_to_page2.sizePolicy().hasHeightForWidth())
        self.go_to_page2.setSizePolicy(sizePolicy2)
        palette33 = QPalette()
        self.go_to_page2.setPalette(palette33)
        self.go_to_page2.setIcon(icon4)
        self.go_to_page2.setIconSize(QSize(44, 44))

        self.verticalLayout_12.addWidget(self.go_to_page2)

        self.installation_pages.addWidget(self.page_1)
        self.page_2 = QWidget()
        self.page_2.setObjectName(u"page_2")
        self.verticalLayout_13 = QVBoxLayout(self.page_2)
        self.verticalLayout_13.setObjectName(u"verticalLayout_13")
        self.label_11 = QLabel(self.page_2)
        self.label_11.setObjectName(u"label_11")
        palette34 = QPalette()
        self.label_11.setPalette(palette34)
        self.label_11.setFont(font2)

        self.verticalLayout_13.addWidget(self.label_11)

        self.domain = QLabel(self.page_2)
        self.domain.setObjectName(u"domain")
        palette38 = QPalette()
        self.domain.setPalette(palette38)

        self.verticalLayout_13.addWidget(self.domain)
        
        font4 = QFont()
        font4.setPointSize(10)
        
        self.domain_frame = QFrame(self.page_2)
        self.domain_frame.setObjectName(u"domain_frame")
        palette39 = QPalette()
        self.domain_frame.setPalette(palette39)
        self.domain_frame.setFont(font3)
        self.domain_frame.setFrameShape(QFrame.StyledPanel)
        self.domain_frame.setFrameShadow(QFrame.Raised)
        self.horizontalLayout_101 = QHBoxLayout(self.domain_frame)
        self.horizontalLayout_101.setObjectName(u"horizontalLayout_101")
        self.domain_value = QLineEdit(self.domain_frame)
        self.domain_value.setObjectName(u"domain_value")
        palette40 = QPalette()
        self.domain_value.setPalette(palette40)
        self.domain_value.setFont(font4)

        self.horizontalLayout_101.addWidget(self.domain_value)


        self.verticalLayout_13.addWidget(self.domain_frame)

        self.go_to_page3 = QPushButton(self.page_2)
        self.go_to_page3.setObjectName(u"go_to_page3")
        sizePolicy2.setHeightForWidth(self.go_to_page3.sizePolicy().hasHeightForWidth())
        self.go_to_page3.setSizePolicy(sizePolicy2)
        palette41 = QPalette()
        self.go_to_page3.setPalette(palette41)
        self.go_to_page3.setIcon(icon4)
        self.go_to_page3.setIconSize(QSize(44, 44))

        self.verticalLayout_13.addWidget(self.go_to_page3)

        self.installation_pages.addWidget(self.page_2)
        self.page_3 = QWidget()
        self.page_3.setObjectName(u"page_3")
        self.verticalLayout_14 = QVBoxLayout(self.page_3)
        self.verticalLayout_14.setObjectName(u"verticalLayout_14")
        self.label_111 = QLabel(self.page_3)
        self.label_111.setObjectName(u"label_111")
        palette42 = QPalette()
        self.label_111.setPalette(palette42)
        self.label_111.setFont(font2)

        self.verticalLayout_14.addWidget(self.label_111)

        self.label_10 = QLabel(self.page_3)
        self.label_10.setObjectName(u"label_10")
        palette43 = QPalette()
        self.label_10.setPalette(palette43)
        self.label_10.setWordWrap(True)

        self.verticalLayout_14.addWidget(self.label_10)

        self.install_btn = QPushButton(self.page_3)
        self.install_btn.setObjectName(u"install_btn")
        sizePolicy2.setHeightForWidth(self.install_btn.sizePolicy().hasHeightForWidth())
        self.install_btn.setSizePolicy(sizePolicy2)
        palette44 = QPalette()
        self.install_btn.setPalette(palette44)
        self.install_btn.setIcon(icon4)
        self.install_btn.setIconSize(QSize(44, 44))

        self.verticalLayout_14.addWidget(self.install_btn)

        self.installation_pages.addWidget(self.page_3)
        self.page_4 = QWidget()
        self.page_4.setObjectName(u"page_4")
        self.verticalLayout_15 = QVBoxLayout(self.page_4)
        self.verticalLayout_15.setObjectName(u"verticalLayout_15")
        self.label_13 = QLabel(self.page_4)
        self.label_13.setObjectName(u"label_13")
        palette45 = QPalette()
        self.label_13.setPalette(palette45)
        self.label_13.setFont(font2)

        self.verticalLayout_15.addWidget(self.label_13)

        self.label_12 = QLabel(self.page_4)
        self.label_12.setObjectName(u"label_12")
        palette46 = QPalette()
        self.label_12.setPalette(palette46)

        self.verticalLayout_15.addWidget(self.label_12)

        self.frame_11 = QFrame(self.page_4)
        self.frame_11.setObjectName(u"frame_11")
        palette47 = QPalette()
        self.frame_11.setPalette(palette47)
        self.frame_11.setFrameShape(QFrame.StyledPanel)
        self.frame_11.setFrameShadow(QFrame.Raised)
        self.horizontalLayout_11 = QHBoxLayout(self.frame_11)
        self.horizontalLayout_11.setObjectName(u"horizontalLayout_11")
        self.progressBar = QProgressBar(self.frame_11)
        self.progressBar.setObjectName(u"progressBar")
        self.progressBar.setValue(0)

        self.horizontalLayout_11.addWidget(self.progressBar)


        self.verticalLayout_15.addWidget(self.frame_11)

        self.installation_pages.addWidget(self.page_4)
        self.page_5 = QWidget()
        self.page_5.setObjectName(u"page_5")
        self.verticalLayout_16 = QVBoxLayout(self.page_5)
        self.verticalLayout_16.setObjectName(u"verticalLayout_16")
        self.label_15 = QLabel(self.page_5)
        self.label_15.setObjectName(u"label_15")
        palette48 = QPalette()
        self.label_15.setPalette(palette48)
        self.label_15.setFont(font2)

        self.verticalLayout_16.addWidget(self.label_15)

        self.label_14 = QLabel(self.page_5)
        self.label_14.setObjectName(u"label_14")
        palette49 = QPalette()
        self.label_14.setPalette(palette49)

        self.verticalLayout_16.addWidget(self.label_14)

        self.frame_12 = QFrame(self.page_5)
        self.frame_12.setObjectName(u"frame_12")
        self.frame_12.setFrameShape(QFrame.StyledPanel)
        self.frame_12.setFrameShadow(QFrame.Raised)
        self.horizontalLayout_12 = QHBoxLayout(self.frame_12)
        self.horizontalLayout_12.setObjectName(u"horizontalLayout_12")
        self.finish_wizard_btn = QPushButton(self.frame_12)
        self.finish_wizard_btn.setObjectName(u"finish_wizard_btn")
        palette50 = QPalette()
        self.finish_wizard_btn.setPalette(palette50)
        icon5 = QIcon()
        icon5.addFile(u"../../../../root/.designer/backup/\n"
"                                                              :/icons/icons/arrow-right-circle.svg", QSize(), QIcon.Normal, QIcon.Off)
        self.finish_wizard_btn.setIcon(icon5)
        self.finish_wizard_btn.setIconSize(QSize(44, 44))

        self.horizontalLayout_12.addWidget(self.finish_wizard_btn)


        self.verticalLayout_16.addWidget(self.frame_12)

        self.installation_pages.addWidget(self.page_5)

        self.verticalLayout_11.addWidget(self.installation_pages)

        self.scrollArea.setWidget(self.scrollAreaWidgetContents)

        self.verticalLayout_10.addWidget(self.scrollArea)


        self.horizontalLayout_7.addWidget(self.pages)


        self.verticalLayout_5.addWidget(self.middle_widget)

        self.progress_indicator_frame = QFrame(self.main_contents)
        self.progress_indicator_frame.setObjectName(u"progress_indicator_frame")
        self.progress_indicator_frame.setMaximumSize(QSize(16777215, 60))
        palette51 = QPalette()
        self.progress_indicator_frame.setPalette(palette51)
        self.progress_indicator_frame.setFrameShape(QFrame.StyledPanel)
        self.progress_indicator_frame.setFrameShadow(QFrame.Raised)
        self.verticalLayout_7 = QVBoxLayout(self.progress_indicator_frame)
        self.verticalLayout_7.setObjectName(u"verticalLayout_7")
        self.progress_indicator = FormProgressIndicator(self.progress_indicator_frame)
        self.progress_indicator.setObjectName(u"progress_indicator")
        self.progress_indicator.setMinimumSize(QSize(0, 50))
        palette52 = QPalette()
        self.progress_indicator.setPalette(palette52)

        self.verticalLayout_7.addWidget(self.progress_indicator)


        self.verticalLayout_5.addWidget(self.progress_indicator_frame, 0, Qt.AlignHCenter)


        self.verticalLayout_2.addWidget(self.main_contents)


        self.verticalLayout.addWidget(self.main_body)

        MainWindow.setCentralWidget(self.centralwidget)

        self.retranslateUi(MainWindow)

        self.installation_pages.setCurrentIndex(0)


        QMetaObject.connectSlotsByName(MainWindow)
    # setupUi

    def retranslateUi(self, MainWindow):
        MainWindow.setWindowTitle(QCoreApplication.translate("MainWindow", u"MainWindow", None))
        self.license_button.setText("")
        self.window_title.setText(QCoreApplication.translate("MainWindow", u"Aivero Deepserver Setup", None))
        self.minimize_window_button.setText("")
        self.exit_button.setText("")
        self.welcome_message.setText(QCoreApplication.translate("MainWindow", u"Are you sure you want to cancel the installation?", None))
        self.close_exit_wizard.setText("")
        self.close_window_button.setText("")
        self.welcome_message1.setText(QCoreApplication.translate("MainWindow", u"PySide2 License: \n"
"GNU LESSER GENERAL PUBLIC LICENSE\n"
"\n"
"Version 3, 29 June 2007\n"
"\n"
"Copyright \u00a9 2007 Free Software Foundation, Inc. <http://fsf.org/>\n"
"\n"
"Everyone is permitted to copy and distribute verbatim copies of this license document, but changing it is not allowed.\n"
"\n"
"This version of the GNU Lesser General Public License incorporates the terms and conditions of version 3 of the GNU General Public License, supplemented by the additional permissions listed below.\n"
"0. Additional Definitions.\n"
"\n"
"As used herein, \u201cthis License\u201d refers to version 3 of the GNU Lesser General Public License, and the \u201cGNU GPL\u201d refers to version 3 of the GNU General Public License.\n"
"\n"
"\u201cThe Library\u201d refers to a covered work governed by this License, other than an Application or a Combined Work as defined below.\n"
"\n"
"An \u201cApplication\u201d is any work that makes use of an interface provided by the Library, but which is not otherwise based on the Libra"
                        "ry. Defining a subclass of a class defined by the Library is deemed a mode of using an interface provided by the Library.\n"
"\n"
"A \u201cCombined Work\u201d is a work produced by combining or linking an Application with the Library. The particular version of the Library with which the Combined Work was made is also called the \u201cLinked Version\u201d.\n"
"\n"
"The \u201cMinimal Corresponding Source\u201d for a Combined Work means the Corresponding Source for the Combined Work, excluding any source code for portions of the Combined Work that, considered in isolation, are based on the Application, and not on the Linked Version.\n"
"\n"
"The \u201cCorresponding Application Code\u201d for a Combined Work means the object code and/or source code for the Application, including any data and utility programs needed for reproducing the Combined Work from the Application, but excluding the System Libraries of the Combined Work.\n"
"1. Exception to Section 3 of the GNU GPL.\n"
"\n"
"You may convey a covered work unde"
                        "r sections 3 and 4 of this License without being bound by section 3 of the GNU GPL.\n"
"2. Conveying Modified Versions.\n"
"\n"
"If you modify a copy of the Library, and, in your modifications, a facility refers to a function or data to be supplied by an Application that uses the facility (other than as an argument passed when the facility is invoked), then you may convey a copy of the modified version:\n"
"\n"
"    a) under this License, provided that you make a good faith effort to ensure that, in the event an Application does not supply the function or data, the facility still operates, and performs whatever part of its purpose remains meaningful, or\n"
"    b) under the GNU GPL, with none of the additional permissions of this License applicable to that copy.\n"
"\n"
"3. Object Code Incorporating Material from Library Header Files.\n"
"\n"
"The object code form of an Application may incorporate material from a header file that is part of the Library. You may convey such object code under terms of your choic"
                        "e, provided that, if the incorporated material is not limited to numerical parameters, data structure layouts and accessors, or small macros, inline functions and templates (ten or fewer lines in length), you do both of the following:\n"
"\n"
"    a) Give prominent notice with each copy of the object code that the Library is used in it and that the Library and its use are covered by this License.\n"
"    b) Accompany the object code with a copy of the GNU GPL and this license document.\n"
"\n"
"4. Combined Works.\n"
"\n"
"You may convey a Combined Work under terms of your choice that, taken together, effectively do not restrict modification of the portions of the Library contained in the Combined Work and reverse engineering for debugging such modifications, if you also do each of the following:\n"
"\n"
"    a) Give prominent notice with each copy of the Combined Work that the Library is used in it and that the Library and its use are covered by this License.\n"
"    b) Accompany the Combined Work with a copy "
                        "of the GNU GPL and this license document.\n"
"    c) For a Combined Work that displays copyright notices during execution, include the copyright notice for the Library among these notices, as well as a reference directing the user to the copies of the GNU GPL and this license document.\n"
"    d) Do one of the following:\n"
"        0) Convey the Minimal Corresponding Source under the terms of this License, and the Corresponding Application Code in a form suitable for, and under terms that permit, the user to recombine or relink the Application with a modified version of the Linked Version to produce a modified Combined Work, in the manner specified by section 6 of the GNU GPL for conveying Corresponding Source.\n"
"        1) Use a suitable shared library mechanism for linking with the Library. A suitable mechanism is one that (a) uses at run time a copy of the Library already present on the user's computer system, and (b) will operate properly with a modified version of the Library that is interface-compatib"
                        "le with the Linked Version.\n"
"    e) Provide Installation Information, but only if you would otherwise be required to provide such information under section 6 of the GNU GPL, and only to the extent that such information is necessary to install and execute a modified version of the Combined Work produced by recombining or relinking the Application with a modified version of the Linked Version. (If you use option 4d0, the Installation Information must accompany the Minimal Corresponding Source and Corresponding Application Code. If you use option 4d1, you must provide the Installation Information in the manner specified by section 6 of the GNU GPL for conveying Corresponding Source.)\n"
"\n"
"5. Combined Libraries.\n"
"\n"
"You may place library facilities that are a work based on the Library side by side in a single library together with other library facilities that are not Applications and are not covered by this License, and convey such a combined library under terms of your choice, if you do both of the f"
                        "ollowing:\n"
"\n"
"    a) Accompany the combined library with a copy of the same work based on the Library, uncombined with any other library facilities, conveyed under the terms of this License.\n"
"    b) Give prominent notice with the combined library that part of it is a work based on the Library, and explaining where to find the accompanying uncombined form of the same work.\n"
"\n"
"6. Revised Versions of the GNU Lesser General Public License.\n"
"\n"
"The Free Software Foundation may publish revised and/or new versions of the GNU Lesser General Public License from time to time. Such new versions will be similar in spirit to the present version, but may differ in detail to address new problems or concerns.\n"
"\n"
"Each version is given a distinguishing version number. If the Library as you received it specifies that a certain numbered version of the GNU Lesser General Public License \u201cor any later version\u201d applies to it, you have the option of following the terms and conditions either of that p"
                        "ublished version or of any later version published by the Free Software Foundation. If the Library as you received it does not specify a version number of the GNU Lesser General Public License, you may choose any version of the GNU Lesser General Public License ever published by the Free Software Foundation.\n"
"\n"
"If the Library as you received it specifies that a proxy can decide whether future versions of the GNU Lesser General Public License shall apply, that proxy's public statement of acceptance of any version is permanent authorization for you to choose that version for the Library.\n"
"  QT-PyQt-PySide-Custom-Widgets:                      GNU GENERAL PUBLIC LICENSE\n"
"                       Version 3, 29 June 2007\n"
"\n"
" Copyright (C) 2007 Free Software Foundation, Inc. <https://fsf.org/>\n"
" Everyone is permitted to copy and distribute verbatim copies\n"
" of this license document, but changing it is not allowed.\n"
"\n"
"                            Preamble\n"
"\n"
"  The GNU General Public Li"
                        "cense is a free, copyleft license for\n"
"software and other kinds of works.\n"
"\n"
"  The licenses for most software and other practical works are designed\n"
"to take away your freedom to share and change the works.  By contrast,\n"
"the GNU General Public License is intended to guarantee your freedom to\n"
"share and change all versions of a program--to make sure it remains free\n"
"software for all its users.  We, the Free Software Foundation, use the\n"
"GNU General Public License for most of our software; it applies also to\n"
"any other work released this way by its authors.  You can apply it to\n"
"your programs, too.\n"
"\n"
"  When we speak of free software, we are referring to freedom, not\n"
"price.  Our General Public Licenses are designed to make sure that you\n"
"have the freedom to distribute copies of free software (and charge for\n"
"them if you wish), that you receive source code or can get it if you\n"
"want it, that you can change the software or use pieces of it in new\n"
"free programs,"
                        " and that you know you can do these things.\n"
"\n"
"  To protect your rights, we need to prevent others from denying you\n"
"these rights or asking you to surrender the rights.  Therefore, you have\n"
"certain responsibilities if you distribute copies of the software, or if\n"
"you modify it: responsibilities to respect the freedom of others.\n"
"\n"
"  For example, if you distribute copies of such a program, whether\n"
"gratis or for a fee, you must pass on to the recipients the same\n"
"freedoms that you received.  You must make sure that they, too, receive\n"
"or can get the source code.  And you must show them these terms so they\n"
"know their rights.\n"
"\n"
"  Developers that use the GNU GPL protect your rights with two steps:\n"
"(1) assert copyright on the software, and (2) offer you this License\n"
"giving you legal permission to copy, distribute and/or modify it.\n"
"\n"
"  For the developers' and authors' protection, the GPL clearly explains\n"
"that there is no warranty for this free software.  F"
                        "or both users' and\n"
"authors' sake, the GPL requires that modified versions be marked as\n"
"changed, so that their problems will not be attributed erroneously to\n"
"authors of previous versions.\n"
"\n"
"  Some devices are designed to deny users access to install or run\n"
"modified versions of the software inside them, although the manufacturer\n"
"can do so.  This is fundamentally incompatible with the aim of\n"
"protecting users' freedom to change the software.  The systematic\n"
"pattern of such abuse occurs in the area of products for individuals to\n"
"use, which is precisely where it is most unacceptable.  Therefore, we\n"
"have designed this version of the GPL to prohibit the practice for those\n"
"products.  If such problems arise substantially in other domains, we\n"
"stand ready to extend this provision to those domains in future versions\n"
"of the GPL, as needed to protect the freedom of users.\n"
"\n"
"  Finally, every program is threatened constantly by software patents.\n"
"States should no"
                        "t allow patents to restrict development and use of\n"
"software on general-purpose computers, but in those that do, we wish to\n"
"avoid the special danger that patents applied to a free program could\n"
"make it effectively proprietary.  To prevent this, the GPL assures that\n"
"patents cannot be used to render the program non-free.\n"
"\n"
"  The precise terms and conditions for copying, distribution and\n"
"modification follow.\n"
"\n"
"                       TERMS AND CONDITIONS\n"
"\n"
"  0. Definitions.\n"
"\n"
"  \"This License\" refers to version 3 of the GNU General Public License.\n"
"\n"
"  \"Copyright\" also means copyright-like laws that apply to other kinds of\n"
"works, such as semiconductor masks.\n"
"\n"
"  \"The Program\" refers to any copyrightable work licensed under this\n"
"License.  Each licensee is addressed as \"you\".  \"Licensees\" and\n"
"\"recipients\" may be individuals or organizations.\n"
"\n"
"  To \"modify\" a work means to copy from or adapt all or part of the work\n"
"in a f"
                        "ashion requiring copyright permission, other than the making of an\n"
"exact copy.  The resulting work is called a \"modified version\" of the\n"
"earlier work or a work \"based on\" the earlier work.\n"
"\n"
"  A \"covered work\" means either the unmodified Program or a work based\n"
"on the Program.\n"
"\n"
"  To \"propagate\" a work means to do anything with it that, without\n"
"permission, would make you directly or secondarily liable for\n"
"infringement under applicable copyright law, except executing it on a\n"
"computer or modifying a private copy.  Propagation includes copying,\n"
"distribution (with or without modification), making available to the\n"
"public, and in some countries other activities as well.\n"
"\n"
"  To \"convey\" a work means any kind of propagation that enables other\n"
"parties to make or receive copies.  Mere interaction with a user through\n"
"a computer network, with no transfer of a copy, is not conveying.\n"
"\n"
"  An interactive user interface displays \"Appropriate Legal "
                        "Notices\"\n"
"to the extent that it includes a convenient and prominently visible\n"
"feature that (1) displays an appropriate copyright notice, and (2)\n"
"tells the user that there is no warranty for the work (except to the\n"
"extent that warranties are provided), that licensees may convey the\n"
"work under this License, and how to view a copy of this License.  If\n"
"the interface presents a list of user commands or options, such as a\n"
"menu, a prominent item in the list meets this criterion.\n"
"\n"
"  1. Source Code.\n"
"\n"
"  The \"source code\" for a work means the preferred form of the work\n"
"for making modifications to it.  \"Object code\" means any non-source\n"
"form of a work.\n"
"\n"
"  A \"Standard Interface\" means an interface that either is an official\n"
"standard defined by a recognized standards body, or, in the case of\n"
"interfaces specified for a particular programming language, one that\n"
"is widely used among developers working in that language.\n"
"\n"
"  The \"System Librari"
                        "es\" of an executable work include anything, other\n"
"than the work as a whole, that (a) is included in the normal form of\n"
"packaging a Major Component, but which is not part of that Major\n"
"Component, and (b) serves only to enable use of the work with that\n"
"Major Component, or to implement a Standard Interface for which an\n"
"implementation is available to the public in source code form.  A\n"
"\"Major Component\", in this context, means a major essential component\n"
"(kernel, window system, and so on) of the specific operating system\n"
"(if any) on which the executable work runs, or a compiler used to\n"
"produce the work, or an object code interpreter used to run it.\n"
"\n"
"  The \"Corresponding Source\" for a work in object code form means all\n"
"the source code needed to generate, install, and (for an executable\n"
"work) run the object code and to modify the work, including scripts to\n"
"control those activities.  However, it does not include the work's\n"
"System Libraries, or general-pu"
                        "rpose tools or generally available free\n"
"programs which are used unmodified in performing those activities but\n"
"which are not part of the work.  For example, Corresponding Source\n"
"includes interface definition files associated with source files for\n"
"the work, and the source code for shared libraries and dynamically\n"
"linked subprograms that the work is specifically designed to require,\n"
"such as by intimate data communication or control flow between those\n"
"subprograms and other parts of the work.\n"
"\n"
"  The Corresponding Source need not include anything that users\n"
"can regenerate automatically from other parts of the Corresponding\n"
"Source.\n"
"\n"
"  The Corresponding Source for a work in source code form is that\n"
"same work.\n"
"\n"
"  2. Basic Permissions.\n"
"\n"
"  All rights granted under this License are granted for the term of\n"
"copyright on the Program, and are irrevocable provided the stated\n"
"conditions are met.  This License explicitly affirms your unlimited\n"
"pe"
                        "rmission to run the unmodified Program.  The output from running a\n"
"covered work is covered by this License only if the output, given its\n"
"content, constitutes a covered work.  This License acknowledges your\n"
"rights of fair use or other equivalent, as provided by copyright law.\n"
"\n"
"  You may make, run and propagate covered works that you do not\n"
"convey, without conditions so long as your license otherwise remains\n"
"in force.  You may convey covered works to others for the sole purpose\n"
"of having them make modifications exclusively for you, or provide you\n"
"with facilities for running those works, provided that you comply with\n"
"the terms of this License in conveying all material for which you do\n"
"not control copyright.  Those thus making or running the covered works\n"
"for you must do so exclusively on your behalf, under your direction\n"
"and control, on terms that prohibit them from making any copies of\n"
"your copyrighted material outside their relationship with you.\n"
"\n"
""
                        "  Conveying under any other circumstances is permitted solely under\n"
"the conditions stated below.  Sublicensing is not allowed; section 10\n"
"makes it unnecessary.\n"
"\n"
"  3. Protecting Users' Legal Rights From Anti-Circumvention Law.\n"
"\n"
"  No covered work shall be deemed part of an effective technological\n"
"measure under any applicable law fulfilling obligations under article\n"
"11 of the WIPO copyright treaty adopted on 20 December 1996, or\n"
"similar laws prohibiting or restricting circumvention of such\n"
"measures.\n"
"\n"
"  When you convey a covered work, you waive any legal power to forbid\n"
"circumvention of technological measures to the extent such circumvention\n"
"is effected by exercising rights under this License with respect to\n"
"the covered work, and you disclaim any intention to limit operation or\n"
"modification of the work as a means of enforcing, against the work's\n"
"users, your or third parties' legal rights to forbid circumvention of\n"
"technological measures.\n"
"\n"
""
                        "  4. Conveying Verbatim Copies.\n"
"\n"
"  You may convey verbatim copies of the Program's source code as you\n"
"receive it, in any medium, provided that you conspicuously and\n"
"appropriately publish on each copy an appropriate copyright notice;\n"
"keep intact all notices stating that this License and any\n"
"non-permissive terms added in accord with section 7 apply to the code;\n"
"keep intact all notices of the absence of any warranty; and give all\n"
"recipients a copy of this License along with the Program.\n"
"\n"
"  You may charge any price or no price for each copy that you convey,\n"
"and you may offer support or warranty protection for a fee.\n"
"\n"
"  5. Conveying Modified Source Versions.\n"
"\n"
"  You may convey a work based on the Program, or the modifications to\n"
"produce it from the Program, in the form of source code under the\n"
"terms of section 4, provided that you also meet all of these conditions:\n"
"\n"
"    a) The work must carry prominent notices stating that you modified\n"
" "
                        "   it, and giving a relevant date.\n"
"\n"
"    b) The work must carry prominent notices stating that it is\n"
"    released under this License and any conditions added under section\n"
"    7.  This requirement modifies the requirement in section 4 to\n"
"    \"keep intact all notices\".\n"
"\n"
"    c) You must license the entire work, as a whole, under this\n"
"    License to anyone who comes into possession of a copy.  This\n"
"    License will therefore apply, along with any applicable section 7\n"
"    additional terms, to the whole of the work, and all its parts,\n"
"    regardless of how they are packaged.  This License gives no\n"
"    permission to license the work in any other way, but it does not\n"
"    invalidate such permission if you have separately received it.\n"
"\n"
"    d) If the work has interactive user interfaces, each must display\n"
"    Appropriate Legal Notices; however, if the Program has interactive\n"
"    interfaces that do not display Appropriate Legal Notices, your\n"
"    wor"
                        "k need not make them do so.\n"
"\n"
"  A compilation of a covered work with other separate and independent\n"
"works, which are not by their nature extensions of the covered work,\n"
"and which are not combined with it such as to form a larger program,\n"
"in or on a volume of a storage or distribution medium, is called an\n"
"\"aggregate\" if the compilation and its resulting copyright are not\n"
"used to limit the access or legal rights of the compilation's users\n"
"beyond what the individual works permit.  Inclusion of a covered work\n"
"in an aggregate does not cause this License to apply to the other\n"
"parts of the aggregate.\n"
"\n"
"  6. Conveying Non-Source Forms.\n"
"\n"
"  You may convey a covered work in object code form under the terms\n"
"of sections 4 and 5, provided that you also convey the\n"
"machine-readable Corresponding Source under the terms of this License,\n"
"in one of these ways:\n"
"\n"
"    a) Convey the object code in, or embodied in, a physical product\n"
"    (including a physi"
                        "cal distribution medium), accompanied by the\n"
"    Corresponding Source fixed on a durable physical medium\n"
"    customarily used for software interchange.\n"
"\n"
"    b) Convey the object code in, or embodied in, a physical product\n"
"    (including a physical distribution medium), accompanied by a\n"
"    written offer, valid for at least three years and valid for as\n"
"    long as you offer spare parts or customer support for that product\n"
"    model, to give anyone who possesses the object code either (1) a\n"
"    copy of the Corresponding Source for all the software in the\n"
"    product that is covered by this License, on a durable physical\n"
"    medium customarily used for software interchange, for a price no\n"
"    more than your reasonable cost of physically performing this\n"
"    conveying of source, or (2) access to copy the\n"
"    Corresponding Source from a network server at no charge.\n"
"\n"
"    c) Convey individual copies of the object code with a copy of the\n"
"    written of"
                        "fer to provide the Corresponding Source.  This\n"
"    alternative is allowed only occasionally and noncommercially, and\n"
"    only if you received the object code with such an offer, in accord\n"
"    with subsection 6b.\n"
"\n"
"    d) Convey the object code by offering access from a designated\n"
"    place (gratis or for a charge), and offer equivalent access to the\n"
"    Corresponding Source in the same way through the same place at no\n"
"    further charge.  You need not require recipients to copy the\n"
"    Corresponding Source along with the object code.  If the place to\n"
"    copy the object code is a network server, the Corresponding Source\n"
"    may be on a different server (operated by you or a third party)\n"
"    that supports equivalent copying facilities, provided you maintain\n"
"    clear directions next to the object code saying where to find the\n"
"    Corresponding Source.  Regardless of what server hosts the\n"
"    Corresponding Source, you remain obligated to ensure that it i"
                        "s\n"
"    available for as long as needed to satisfy these requirements.\n"
"\n"
"    e) Convey the object code using peer-to-peer transmission, provided\n"
"    you inform other peers where the object code and Corresponding\n"
"    Source of the work are being offered to the general public at no\n"
"    charge under subsection 6d.\n"
"\n"
"  A separable portion of the object code, whose source code is excluded\n"
"from the Corresponding Source as a System Library, need not be\n"
"included in conveying the object code work.\n"
"\n"
"  A \"User Product\" is either (1) a \"consumer product\", which means any\n"
"tangible personal property which is normally used for personal, family,\n"
"or household purposes, or (2) anything designed or sold for incorporation\n"
"into a dwelling.  In determining whether a product is a consumer product,\n"
"doubtful cases shall be resolved in favor of coverage.  For a particular\n"
"product received by a particular user, \"normally used\" refers to a\n"
"typical or common use of "
                        "that class of product, regardless of the status\n"
"of the particular user or of the way in which the particular user\n"
"actually uses, or expects or is expected to use, the product.  A product\n"
"is a consumer product regardless of whether the product has substantial\n"
"commercial, industrial or non-consumer uses, unless such uses represent\n"
"the only significant mode of use of the product.\n"
"\n"
"  \"Installation Information\" for a User Product means any methods,\n"
"procedures, authorization keys, or other information required to install\n"
"and execute modified versions of a covered work in that User Product from\n"
"a modified version of its Corresponding Source.  The information must\n"
"suffice to ensure that the continued functioning of the modified object\n"
"code is in no case prevented or interfered with solely because\n"
"modification has been made.\n"
"\n"
"  If you convey an object code work under this section in, or with, or\n"
"specifically for use in, a User Product, and the conveying "
                        "occurs as\n"
"part of a transaction in which the right of possession and use of the\n"
"User Product is transferred to the recipient in perpetuity or for a\n"
"fixed term (regardless of how the transaction is characterized), the\n"
"Corresponding Source conveyed under this section must be accompanied\n"
"by the Installation Information.  But this requirement does not apply\n"
"if neither you nor any third party retains the ability to install\n"
"modified object code on the User Product (for example, the work has\n"
"been installed in ROM).\n"
"\n"
"  The requirement to provide Installation Information does not include a\n"
"requirement to continue to provide support service, warranty, or updates\n"
"for a work that has been modified or installed by the recipient, or for\n"
"the User Product in which it has been modified or installed.  Access to a\n"
"network may be denied when the modification itself materially and\n"
"adversely affects the operation of the network or violates the rules and\n"
"protocols for c"
                        "ommunication across the network.\n"
"\n"
"  Corresponding Source conveyed, and Installation Information provided,\n"
"in accord with this section must be in a format that is publicly\n"
"documented (and with an implementation available to the public in\n"
"source code form), and must require no special password or key for\n"
"unpacking, reading or copying.\n"
"\n"
"  7. Additional Terms.\n"
"\n"
"  \"Additional permissions\" are terms that supplement the terms of this\n"
"License by making exceptions from one or more of its conditions.\n"
"Additional permissions that are applicable to the entire Program shall\n"
"be treated as though they were included in this License, to the extent\n"
"that they are valid under applicable law.  If additional permissions\n"
"apply only to part of the Program, that part may be used separately\n"
"under those permissions, but the entire Program remains governed by\n"
"this License without regard to the additional permissions.\n"
"\n"
"  When you convey a copy of a covered work, "
                        "you may at your option\n"
"remove any additional permissions from that copy, or from any part of\n"
"it.  (Additional permissions may be written to require their own\n"
"removal in certain cases when you modify the work.)  You may place\n"
"additional permissions on material, added by you to a covered work,\n"
"for which you have or can give appropriate copyright permission.\n"
"\n"
"  Notwithstanding any other provision of this License, for material you\n"
"add to a covered work, you may (if authorized by the copyright holders of\n"
"that material) supplement the terms of this License with terms:\n"
"\n"
"    a) Disclaiming warranty or limiting liability differently from the\n"
"    terms of sections 15 and 16 of this License; or\n"
"\n"
"    b) Requiring preservation of specified reasonable legal notices or\n"
"    author attributions in that material or in the Appropriate Legal\n"
"    Notices displayed by works containing it; or\n"
"\n"
"    c) Prohibiting misrepresentation of the origin of that material, "
                        "or\n"
"    requiring that modified versions of such material be marked in\n"
"    reasonable ways as different from the original version; or\n"
"\n"
"    d) Limiting the use for publicity purposes of names of licensors or\n"
"    authors of the material; or\n"
"\n"
"    e) Declining to grant rights under trademark law for use of some\n"
"    trade names, trademarks, or service marks; or\n"
"\n"
"    f) Requiring indemnification of licensors and authors of that\n"
"    material by anyone who conveys the material (or modified versions of\n"
"    it) with contractual assumptions of liability to the recipient, for\n"
"    any liability that these contractual assumptions directly impose on\n"
"    those licensors and authors.\n"
"\n"
"  All other non-permissive additional terms are considered \"further\n"
"restrictions\" within the meaning of section 10.  If the Program as you\n"
"received it, or any part of it, contains a notice stating that it is\n"
"governed by this License along with a term that is a further\n"
""
                        "restriction, you may remove that term.  If a license document contains\n"
"a further restriction but permits relicensing or conveying under this\n"
"License, you may add to a covered work material governed by the terms\n"
"of that license document, provided that the further restriction does\n"
"not survive such relicensing or conveying.\n"
"\n"
"  If you add terms to a covered work in accord with this section, you\n"
"must place, in the relevant source files, a statement of the\n"
"additional terms that apply to those files, or a notice indicating\n"
"where to find the applicable terms.\n"
"\n"
"  Additional terms, permissive or non-permissive, may be stated in the\n"
"form of a separately written license, or stated as exceptions;\n"
"the above requirements apply either way.\n"
"\n"
"  8. Termination.\n"
"\n"
"  You may not propagate or modify a covered work except as expressly\n"
"provided under this License.  Any attempt otherwise to propagate or\n"
"modify it is void, and will automatically terminate your r"
                        "ights under\n"
"this License (including any patent licenses granted under the third\n"
"paragraph of section 11).\n"
"\n"
"  However, if you cease all violation of this License, then your\n"
"license from a particular copyright holder is reinstated (a)\n"
"provisionally, unless and until the copyright holder explicitly and\n"
"finally terminates your license, and (b) permanently, if the copyright\n"
"holder fails to notify you of the violation by some reasonable means\n"
"prior to 60 days after the cessation.\n"
"\n"
"  Moreover, your license from a particular copyright holder is\n"
"reinstated permanently if the copyright holder notifies you of the\n"
"violation by some reasonable means, this is the first time you have\n"
"received notice of violation of this License (for any work) from that\n"
"copyright holder, and you cure the violation prior to 30 days after\n"
"your receipt of the notice.\n"
"\n"
"  Termination of your rights under this section does not terminate the\n"
"licenses of parties who have rece"
                        "ived copies or rights from you under\n"
"this License.  If your rights have been terminated and not permanently\n"
"reinstated, you do not qualify to receive new licenses for the same\n"
"material under section 10.\n"
"\n"
"  9. Acceptance Not Required for Having Copies.\n"
"\n"
"  You are not required to accept this License in order to receive or\n"
"run a copy of the Program.  Ancillary propagation of a covered work\n"
"occurring solely as a consequence of using peer-to-peer transmission\n"
"to receive a copy likewise does not require acceptance.  However,\n"
"nothing other than this License grants you permission to propagate or\n"
"modify any covered work.  These actions infringe copyright if you do\n"
"not accept this License.  Therefore, by modifying or propagating a\n"
"covered work, you indicate your acceptance of this License to do so.\n"
"\n"
"  10. Automatic Licensing of Downstream Recipients.\n"
"\n"
"  Each time you convey a covered work, the recipient automatically\n"
"receives a license from the "
                        "original licensors, to run, modify and\n"
"propagate that work, subject to this License.  You are not responsible\n"
"for enforcing compliance by third parties with this License.\n"
"\n"
"  An \"entity transaction\" is a transaction transferring control of an\n"
"organization, or substantially all assets of one, or subdividing an\n"
"organization, or merging organizations.  If propagation of a covered\n"
"work results from an entity transaction, each party to that\n"
"transaction who receives a copy of the work also receives whatever\n"
"licenses to the work the party's predecessor in interest had or could\n"
"give under the previous paragraph, plus a right to possession of the\n"
"Corresponding Source of the work from the predecessor in interest, if\n"
"the predecessor has it or can get it with reasonable efforts.\n"
"\n"
"  You may not impose any further restrictions on the exercise of the\n"
"rights granted or affirmed under this License.  For example, you may\n"
"not impose a license fee, royalty, or other"
                        " charge for exercise of\n"
"rights granted under this License, and you may not initiate litigation\n"
"(including a cross-claim or counterclaim in a lawsuit) alleging that\n"
"any patent claim is infringed by making, using, selling, offering for\n"
"sale, or importing the Program or any portion of it.\n"
"\n"
"  11. Patents.\n"
"\n"
"  A \"contributor\" is a copyright holder who authorizes use under this\n"
"License of the Program or a work on which the Program is based.  The\n"
"work thus licensed is called the contributor's \"contributor version\".\n"
"\n"
"  A contributor's \"essential patent claims\" are all patent claims\n"
"owned or controlled by the contributor, whether already acquired or\n"
"hereafter acqui", None))
        self.aivero_logo.setText("")
        self.label_4.setText(QCoreApplication.translate("MainWindow", u"Welcome to the Aivero DeepServer!", None))
        self.label_5.setText(QCoreApplication.translate("MainWindow", u"Click Start in order to begin the installation", None))
        self.start_wizard_btn.setText(QCoreApplication.translate("MainWindow", u"Start", None))
        self.label_9.setText(QCoreApplication.translate("MainWindow", u"Please carefully read these instructions!", None))
        self.label_16.setText(QCoreApplication.translate("MainWindow", u"This tool will guide you on setting up and installing your Deepserver in this computer. While the installation takes place, do not close this window, shutdown the computer, or disconnect it from the internet. On the next page, you will need to insert information regarding your DeepServer setup.", None))
        self.go_to_page2.setText(QCoreApplication.translate("MainWindow", u"Next", None))
        self.label_11.setText(QCoreApplication.translate("MainWindow", u"DeepServer Information", None))
        self.domain.setText(QCoreApplication.translate("MainWindow", u"Domain of the DeepServer", None))
        self.domain_value.setPlaceholderText(QCoreApplication.translate("MainWindow", u"Type your domain name here. Example: aivero.lan", None))
        self.go_to_page3.setText(QCoreApplication.translate("MainWindow", u"Next", None))
        self.label_111.setText(QCoreApplication.translate("MainWindow", u"Starting installation", None))
        self.label_10.setText(QCoreApplication.translate("MainWindow", u"The system is ready to install the DeepServer. Press the button below to begin", None))
        self.install_btn.setText(QCoreApplication.translate("MainWindow", u"Install", None))
        self.label_13.setText(QCoreApplication.translate("MainWindow", u"Currently installing DeepServer", None))
        self.label_12.setText(QCoreApplication.translate("MainWindow", u"Progress", None))
        self.label_15.setText(QCoreApplication.translate("MainWindow", u"The installation is finished.", None))
        self.label_14.setText(QCoreApplication.translate("MainWindow", u"Deepserver installed successfully!", None))
        self.finish_wizard_btn.setText(QCoreApplication.translate("MainWindow", u"Finish", None))
    # retranslateUi

