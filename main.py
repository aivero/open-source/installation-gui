import sys
import subprocess
import re
import fnmatch
import os
import shutil
import argparse

from PySide2 import QtCore
from PySide2.QtCore import *
from ui_interface import *
from Custom_Widgets.Widgets import *
from datetime import datetime

activeWidget = 1
installation_succeed = False
installation_failed = False
load_ds_images = False
load_open_balena = False
load_open_balena_fleets = False
airgapped = False

class ThreadedInstall(QtCore.QThread):
    def __init__(self, domain_name) -> None:
        super().__init__()
        print(f"Domain: {domain_name}")
        self.domain_name = domain_name
        self.time = datetime.now()

    progress_value = QtCore.Signal(int)
    current_task = QtCore.Signal(str)

    def run(self):
        """Run the Ansible playbook and emit signals on the progress"""
        global installation_succeed
        global installation_failed

        logs_dir = "../../aivero_deepserver_install_logs"
        current_log = "deepserver_install_logfile_" + str(self.time) + ".log"
        if not os.path.exists(logs_dir):
            os.mkdir(logs_dir)

        log_file = open(current_log, "x") 


        p = subprocess.Popen(
            'ansible-playbook deep-server-update-ansible.yml --extra-vars "domain_name='
            + self.domain_name
            + ' load_ds_images='
            + str(load_ds_images)
            + ' load_open_balena='
            + str(load_open_balena)
            + ' load_open_balena_fleets='
            + str(load_open_balena_fleets)
            + ' airgapped='
            + str(airgapped)
            + '"',
            shell=True,
            stdout=subprocess.PIPE,
            stderr=subprocess.STDOUT,
        )
        steps = 35
        progress_percent = -1
        for line in p.stdout:
            line = line.decode(
                errors="replace" if (sys.version_info) < (3, 5) else "backslashreplace"
            ).rstrip()
            print(line)
            log_file.write(line)
            if line.find("TASK") != -1:
                filtered = re.findall(r"\d+", line)
                if len(filtered) != 0:
                    progress_percent = (int(filtered[0]) / steps) * 100
                    self.progress_value.emit(int(progress_percent))
                    step_output = re.search("\[(.*)\]", line)
                    self.current_task.emit(str(step_output.group(0)))

        log_file.close()
        
        if progress_percent == 100:
            installation_succeed = True

        else:
            shutil.move(current_log, logs_dir)
            installation_failed = True


class MainWindow(QMainWindow):
    def __init__(self):
        """Initalize UI and setup callbacks"""
        QMainWindow.__init__(self)
        self.ui = Ui_MainWindow()
        self.ui.setupUi(self)
        self.ui.go_to_page3.setDisabled(True)
        self.wildcards = "*.*"

        loadJsonStyle(self, self.ui)

        self.ui.progress_indicator.selectFormProgressIndicatorTheme(3)
        self.ui.progress_indicator.updateFormProgressIndicator(
            formProgressCount=5,
            formProgressAnimationDuration=1000,
            height=40,
            startPercentage=1,
        )

        self.ui.start_wizard_btn.clicked.connect(lambda: self.startWizard())
        self.ui.welcome.expandMenu()

        self.ui.exit_button.clicked.connect(lambda: self.showExitDialogue())
        self.ui.license_button.clicked.connect(lambda: self.showLicenseDialogue())
        self.ui.close_exit_wizard.clicked.connect(lambda: self.hideExitDialogue())

        self.ui.go_to_page2.clicked.connect(lambda: self.nxtSlide())
        self.ui.go_to_page3.clicked.connect(lambda: self.nxtSlide())

        self.ui.install_btn.clicked.connect(lambda: self.triggerInstall())
        self.ui.finish_wizard_btn.clicked.connect(lambda: self.close())

        self.timer = QtCore.QTimer()
        self.timer.timeout.connect(self.validateInstallation)
        self.timer.timeout.connect(self.checkUserInput)
        self.timer.start(100)

        self.show()

    def showLicenseDialogue(self):
        """Show licensing window or close it"""
        if self.ui.licensing_container.collapsed:
            self.ui.main_contents.collapseMenu()
            self.ui.exit_container.collapseMenu()
            self.ui.licensing_container.expandMenu()

        else:
            self.ui.licensing_container.collapseMenu()
            self.ui.exit_container.collapseMenu()
            self.ui.main_contents.expandMenu()

    def showExitDialogue(self):
        """Show exit window or close it"""
        if self.ui.exit_container.collapsed:
            self.ui.main_contents.collapseMenu()
            self.ui.licensing_container.collapseMenu()
            self.ui.exit_container.expandMenu()

        else:
            self.hideExitDialogue()

    def hideExitDialogue(self):
        self.ui.exit_container.collapseMenu()
        self.ui.licensing_container.collapseMenu()
        self.ui.main_contents.expandMenu()

    def checkUserInput(self):
        if (
            len(fnmatch.filter([self.ui.domain_value.text()], self.wildcards))
            == 1
        ):
            self.ui.go_to_page3.setEnabled(True)
        else:
            self.ui.go_to_page3.setDisabled(True)

    # START WIZARD
    def startWizard(self):
        """Open up UI welcome page"""
        global activeWidget
        activeWidget = 1
        self.ui.installation_pages.setCurrentWidget(self.ui.page_1)
        self.ui.pages.expandMenu()
        self.ui.welcome.collapseMenu()

        currentPerc = (activeWidget / 5) * 100
        self.ui.progress_indicator.animateFormProgress(currentPerc)

    # STOP WIZARD
    def stopWizard(self):
        """Stop installation"""
        self.ui.pages.collapseMenu()
        self.ui.welcome.expandMenu()

        self.ui.progress_indicator.animateFormProgress(100)

    def triggerInstall(self):
        """Triggers installation of DS by calling an Ansible playbook on a different thread"""
        self.nxtSlide()
        self.thread = ThreadedInstall(
            self.ui.domain_value.text()
        )
        self.thread.progress_value.connect(self.update_progress_bar)
        self.thread.current_task.connect(self.update_steps)
        self.thread.start()

    def update_progress_bar(self, value):
        """Updates the progress bar based on the Ansible playbook steps

        Args:
            value (QtCore.Signal(int)): Current progress of the DS installation
        """
        self.ui.progressBar.setValue(value)

    def update_steps(self, step):
        """Updates which step the installation is currently at

        Args:
            step (QtCore.Signal(str)): Current step of the DS installation (output from Ansible Playbook)
        """
        self.ui.label_12.setText(step)

    def nxtSlide(self):
        """Switches to next page and increases progress"""
        global activeWidget
        if self.ui.installation_pages.currentIndex() < 4:
            activeWidget = self.ui.installation_pages.currentIndex() + 2

        currentPerc = (activeWidget / 5) * 100
        self.ui.progress_indicator.animateFormProgress(currentPerc)
        self.ui.installation_pages.slideToNextWidget()

    def validateInstallation(self):
        """Validates whether the installation was successful and switches to the next page.
        Checks against global variables modified in the ThreadedInstall class.
        """
        global activeWidget
        global installation_failed
        global installation_succeed
        if installation_failed:
            self.ui.label_14.setText("Installation failed.")

        if installation_failed or installation_succeed:
            if self.ui.installation_pages.currentIndex() < 4:
                activeWidget = self.ui.installation_pages.currentIndex() + 2

            currentPerc = (activeWidget / 5) * 100
            self.ui.progress_indicator.animateFormProgress(currentPerc)
            self.ui.installation_pages.slideToNextWidget()
            installation_succeed, installation_failed = False, False


if __name__ == "__main__":

    parser = argparse.ArgumentParser()
    parser.add_argument("--load-ds-images", action="store_true", help="Set whether there is an update package for airgapped ds-images")
    parser.add_argument("--load-open-balena", action="store_true", help="Set whether there is an update package for airgapped load_open_balena")
    parser.add_argument("--load-open-balena-fleets", action="store_true", help="Set whether there is an update package for airgapped load_open_balena_fleets")
    parser.add_argument("--airgapped", action="store_true", help="Set whether setup is airgapped")
    args = parser.parse_args()

    load_ds_images=args.load_ds_images
    load_open_balena=args.load_open_balena
    load_open_balena_fleets=args.load_open_balena_fleets
    airgapped=args.airgapped
    app = QApplication(sys.argv)
    window = MainWindow()
    sys.exit(app.exec_())
